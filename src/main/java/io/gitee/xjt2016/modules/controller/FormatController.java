package io.gitee.xjt2016.modules.controller;

import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.service.ResourceService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@Controller
@RequestMapping(value = "/format")
public class FormatController extends BaseController {

    public static final String module = "format";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    ResourceService service;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);
    }

    @GetMapping(value = {"/sql"})
    public String sql() throws Exception {
        return pathPrefix + "/sql";
    }

    @GetMapping(value = {"/json"})
    public String json() throws Exception {
        return pathPrefix + "/json";
    }
}

#!/bin/bash
JAVA_OPTS="-Xms128m -Xmx1G -XX:+UseParNewGC -XX:+UseConcMarkSweepGC "
cd /code && java ${JAVA_OPTS} org.springframework.boot.loader.JarLauncher
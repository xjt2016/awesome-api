package io.gitee.xjt2016.modules.service.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.common.utils.SplitUtil;
import io.gitee.xjt2016.modules.domain.sys.Role;
import io.gitee.xjt2016.modules.mapper.sys.RoleMapper;
import io.gitee.xjt2016.modules.service.ResourceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@Service
public class RoleService extends ServiceImpl<RoleMapper, Role> {

    @Resource
    ResourceService resourceService;

    public Set<String> findRoles(Long... roleIds) {
        Set<String> roles = new HashSet<>();
        for (Long roleId : roleIds) {
            Role role = this.getById(roleId);
            if (role != null) {
                roles.add(role.getName());
            }
        }
        return roles;
    }

    public Set<String> findPermissions(Long[] roleIds) {
        Set<Long> resourceIds = new HashSet<>();
        for (Long roleId : roleIds) {
            Role role = getById(roleId);
            if (role != null) {
                resourceIds.addAll(SplitUtil.split2LongList(role.getResourceIds()));
            }
        }
        return resourceService.findPermissions(resourceIds);
    }

}

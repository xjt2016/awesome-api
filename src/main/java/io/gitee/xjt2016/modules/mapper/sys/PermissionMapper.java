package io.gitee.xjt2016.modules.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.sys.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
}

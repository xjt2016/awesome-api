package io.gitee.xjt2016;

import cn.hutool.core.util.RandomUtil;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Log4j2
public class FutureTest {
    public static final ExecutorService THREADPOOL = new ThreadPoolExecutor(2, 4, 60L, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(50), new ThreadFactoryBuilder().setNameFormat("replace-pool-%d").build());


    public static void main(String[] args) {
        String params = "ksxdDataSource,yzDataSource,fxgDataSource,weimaoDataSource";
        List<Future<Integer>> futures = Arrays.stream(params.split(","))
                .map(StringUtils::trim)
//                .filter(SpringContextUtil::containsBean)
                .distinct()
                .map(s -> THREADPOOL.submit(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(RandomUtil.randomInt(5));

//                        dataMoveCorn(SpringContextUtil.getBean(s, DruidDataSource.class), s);
                        return 1;
                    } catch (Throwable throwable) {
                        return 0;
                    }
                })).collect(Collectors.toList());

        //阻塞获取迁移结果
        for (Future<Integer> future : futures) {
            try {
                System.out.println(future.get());
            } catch (Throwable e) {
                log.error(e.getMessage(), e);
            }
        }
        System.out.println("done");
    }
}

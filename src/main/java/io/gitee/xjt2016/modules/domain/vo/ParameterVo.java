package io.gitee.xjt2016.modules.domain.vo;

import io.gitee.xjt2016.modules.common.BootStrapTablePage;
import lombok.Data;

@Data
public class ParameterVo extends BootStrapTablePage {
}

package io.gitee.xjt2016.modules.activiti7;

import lombok.extern.log4j.Log4j2;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//https://gitee.com/-/ide/project/flag_yiyi/activiti-web-7.0/edit/master/-/src/test/java/com/imooc/activitiweb/Part6_UEL.java

@Log4j2
public class ActivitiTaskTest extends Activiti7 {
    //任务查询
    @Test
    public void getTasks() {
        List<Task> list = taskService.createTaskQuery().list();
        for (Task tk : list) {
            log.debug("------task-------");
            log.debug("Id：" + tk.getId());
            log.debug("Name：" + tk.getName());
            log.debug("Assignee：" + tk.getAssignee());
        }
    }

    //查询我的代办任务
    @Test
    public void getTasksByAssignee() {
        List<Task> list = taskService.createTaskQuery()
                .taskAssignee("bajie")
                .list();
        for (Task tk : list) {
            log.debug("Id：" + tk.getId());
            log.debug("Name：" + tk.getName());
            log.debug("Assignee：" + tk.getAssignee());
        }

    }

    //执行任务
    @Test
    public void completeTask() {
        Map<String, Object> params = new HashMap<>();
        params.put("deptLeaderApproved", true);
        taskService.complete("20006", params);
        log.debug("完成任务");
    }

    //拾取任务
    @Test
    public void claimTask() {
        Task task = taskService.createTaskQuery().taskId("1f2a8edf-cefa-11ea-84aa-dcfb4875e032").singleResult();
        taskService.claim("1f2a8edf-cefa-11ea-84aa-dcfb4875e032", "bajie");
    }

    //归还与交办任务
    @Test
    public void setTaskAssignee() {
        Task task = taskService.createTaskQuery().taskId("1f2a8edf-cefa-11ea-84aa-dcfb4875e032").singleResult();
        taskService.setAssignee("1f2a8edf-cefa-11ea-84aa-dcfb4875e032", null);//归还候选任务
        taskService.setAssignee("1f2a8edf-cefa-11ea-84aa-dcfb4875e032", "wukong");//交办
    }
}

package io.gitee.xjt2016;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ResourceTest {
    @Test
    public void test001() {
        String distPath = "/Users/xjt2016/Downloads";
        String key = "cdn.bootcdn.net";
        String path = "/Users/xjt2016/Documents/raycloud/awesome-api/src/main/resources";
        List<File> fileList = FileUtil
                .loopFiles(path, pathname -> FileUtil.readUtf8String(pathname).contains(key));

        Set<String> strings = fileList.stream()
                .flatMap((Function<File, Stream<String>>) file -> FileUtil.readLines(file, "utf-8")
                        .stream()
                        .filter(s -> s.contains(key))).map(s -> {
                    s = StringUtils.trim(s);
                    return StringUtils.substringBetween(s, key, "\"");
                }).collect(Collectors.toSet());

        for (String string : strings) {
            string = "/static" + string;
            System.out.println(string);

            String p = distPath + StringUtils.substringAfter(string, key);
            System.out.println(p);

            System.out.println(HttpUtil.downloadFile(string, p));
        }
    }


}

package io.gitee.xjt2016.modules.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.gitee.xjt2016.modules.domain.Field;
import io.gitee.xjt2016.modules.domain.FieldDataType;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@Log4j2
public class JsonLoop {

    public static String json = "{\"buyerMessage\":\"\",\"buyerNick\":\"山西省吕梁市文水县南庄镇汾曲村\",\"childNum\":0,\"cod\":0,\"consignTime\":0,\"created\":1594263646051,\"isPreSale\":0,\"modified\":1594264102859,\"noLogisticsSend\":true,\"orders\":[{\"isPreSale\":0,\"itemId\":\"336246429553\",\"itemType\":\"2\",\"number\":1,\"orderId\":\"2019100001846486-336246429553\",\"orderNoLogisticsSend\":true,\"outerId\":\"336246429553\",\"outerSkuId\":\"10\",\"payment\":\"14.5\",\"picPath\":\"https://ali-ec.static.yximgs.com/ufile/adsocial/06ffa28b-990b-4549-bbcd-09ecab8d2884.jpg\",\"price\":\"14.5\",\"saleProps\":[{\"key\":\"color\",\"value\":\"规格值\"}],\"skuId\":\"336246433553\",\"skuProperties\":\"颜色:规格值\",\"skuShortProperties\":\"规格值\",\"skuUrl\":\"https://www.kwaishop.com/merchant/shop/order/new?id=336246429553\",\"status\":\"ORDER_FAIL\",\"title\":\"闪电购商品\",\"totalFee\":\"14.5\"}],\"payment\":\"14.5\",\"postFee\":\"0.0\",\"receiverAddress\":\"山西省吕梁市文水县南庄镇汾曲村\",\"receiverCity\":\"吕梁市\",\"receiverCounty\":\"文水县\",\"receiverMobile\":\"15534386423\",\"receiverName\":\"山西省吕梁市文水县南庄镇汾曲村\",\"receiverProvince\":\"山西省\",\"sellerMemo\":\"\",\"status\":\"ORDER_FAIL\",\"sysDiscountFee\":\"0.0\",\"tid\":\"2019100001846486\",\"totalFee\":\"14.5\"}\n";

    public static Long nextId = 0L;

    public List<Field> jsonLoop(Object object, Long pid) {
        JSONObject jsonObject = new JSONObject();
        if (object instanceof JSONArray) {
            jsonObject = (JSONObject) ((JSONArray) object).get(0);
        }
        if (object instanceof JSONObject) {
            jsonObject = (JSONObject) object;
        }

        List<Field> fieldList = new ArrayList<>();

        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            nextId = nextId + 1;
            Field field = new Field();
            field.setId(nextId);
            field.setFid(nextId);
            field.setName(entry.getKey());
            field.setDefaultValue(null);
            field.setDescription("");
            field.setPid(pid);

            fieldList.add(field);

            Object o = entry.getValue();
            if (o instanceof String) {
                field.setDataType(FieldDataType.String.getValue());
            } else if (o instanceof Number) {
                field.setDataType(FieldDataType.Number.getValue());
            } else if (o instanceof Boolean) {
                field.setDataType(FieldDataType.Boolean.getValue());
            } else if (o instanceof JSONObject) {
                field.setDataType(FieldDataType.Object.getValue());
                fieldList.addAll(jsonLoop(o, field.getId()));
            } else if (o instanceof JSONArray) {
                field.setDataType(FieldDataType.Array.getValue());
                fieldList.addAll(jsonLoop(o, field.getId()));
            }
        }
        return fieldList;
    }

    public static void main(String[] args) {
        JSONObject jsonObject = JSON.parseObject(json);
        JsonLoop jsonLoop = new JsonLoop();
        List<Field> fields = jsonLoop.jsonLoop(jsonObject, 0L);
        fields.forEach(new Consumer<Field>() {
            @Override
            public void accept(Field field) {
                field.setAnInterface(null);
            }
        });
        System.out.println(JSON.toJSONString(fields, SerializerFeature.WriteMapNullValue));
    }
}
package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.gitee.xjt2016.modules.domain.sys.ResourceType;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
@TableName(value = "sys_resource")
public class Resource {
    @TableId(type = IdType.AUTO)
    private Long id; //编号
    private String name; //资源名称
    private ResourceType type = ResourceType.menu; //资源类型
    private String url; //资源路径
    private String permission; //权限字符串
    private Long pid; //父编号
    private Boolean available = Boolean.FALSE;

    public boolean isRootNode() {
        return Objects.equals(this.getId(), ROOT_ID);
    }

    public final static Long ROOT_ID = 0L;

    @TableField(exist = false)
    private List<io.gitee.xjt2016.modules.domain.Resource> children;

}

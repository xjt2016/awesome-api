package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Interface {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long functionId;

    @TableField(exist = false)
    private Function function = new Function();

    private String name;
    private String description;
    private String url;

}

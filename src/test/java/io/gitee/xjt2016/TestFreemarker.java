//package io.gitee.xjt2016;
//
//import freemarker.template.Configuration;
//import freemarker.template.Template;
//import freemarker.template.TemplateException;
//import org.apache.commons.io.IOUtils;
//import org.junit.Test;
//import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * TODO
// *
// * @author dsdj
// * @version 1.0
// * @className TestFreemarker
// * @date 2019/2/12 10:30
// **/
//
//public class TestFreemarker {
//    @Test
//    public void testGenerateHtml() throws IOException, TemplateException, URISyntaxException {
//        // 创建配置类
//        Configuration configuration = new Configuration(Configuration.getVersion());
//        // 设置模板路径 toURI()防止路径出现空格
//        String classpath = this.getClass().getResource("/").toURI().getPath();
//        configuration.setDirectoryForTemplateLoading(new File(classpath+"/templates/"));
//        // 设置字符集
//        configuration.setDefaultEncoding("utf-8");
//        // 加载模板
//        Template template = configuration.getTemplate("demo1.ftl");
//        // 数据模型
//        Map<String,Object> map = new HashMap<>();
//        map.put("name", "静态化测试");
//        // 静态化
//        String content = FreeMarkerTemplateUtils.processTemplateIntoString(template,map);
//        // 打印静态化内容
//        System.out.println(content);
//        InputStream inputStream = IOUtils.toInputStream(content);
//        // 输出文件
//        FileOutputStream fileOutputStream = new FileOutputStream(new File("demo1.html"));
//        int copy = IOUtils.copy(inputStream, fileOutputStream);
//
//    }
//}
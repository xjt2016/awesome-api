package io.gitee.xjt2016.modules.domain.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@TableName(value = "sys_organization")
@Data
public class Organization {
    public static final Long root = 0L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //编号
    private String name; //组织机构名称
    private Long pid; //父编号

    private Integer available = 1;
    @TableField(exist = false)
    private String pName;

    public boolean isRootNode() {
        return Objects.equals(root, id);
    }

    @TableField(exist = false)
    private List<Organization> children = new ArrayList<>();
}

package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Parameter {
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField(value = "`key`")
    private String key;
    @TableField(value = "`value`")
    private String value;

}

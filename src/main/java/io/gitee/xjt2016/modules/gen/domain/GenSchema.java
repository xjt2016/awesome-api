package io.gitee.xjt2016.modules.gen.domain;

import com.google.common.collect.Lists;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class GenSchema {

    @NotBlank
    private String basePackageName;//包名 xxx.xx.xx

    @NotBlank
    private String moduleName;//模块名

    @NotBlank
    private String entityName;

    @NotNull
    private GenTable genTable;

    @NotBlank
    private String genPath;

    public List<String> getImportPackages() {
        return genTable.getColumns().stream().map(GenTableColumn::getJavaFieldType).sorted().distinct().collect(Collectors.toList());
    }

    public GenSchema() {
    }

    public GenSchema(GenTable genTable) {
        this.genTable = genTable;
    }

    public String getPackageName(String cat) {
        return Lists.newArrayList(basePackageName, moduleName, cat).stream().filter(StringUtils::isNotBlank).collect(Collectors.joining("."));
    }
}

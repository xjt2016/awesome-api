package io.gitee.xjt2016.modules.domain.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@TableName(value = "sys_user")
public class User {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long orgId; //所属公司
    private String username; //用户名
    private String password; //密码

    @TableField(exist = false)
    private String password2;//确认密码

    private String salt; //加密密码的盐
    private String roleIds; //拥有的角色列表

    @TableField(exist = false)
    private String roleNames; //拥有的角色列表
    private Integer locked = 0;

    private Date created;

    private Date modified;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public List<Long> getRoleIdList() {
        String roleIds = this.getRoleIds();
        if (roleIds == null) {
            roleIds = "";
        }
        return Arrays.stream(roleIds.split(","))
                .filter(StringUtils::isNotBlank)
                .map(Long::valueOf)
                .collect(Collectors.toList());
    }

    public String getCredentialsSalt() {
        return username + salt;
    }
}

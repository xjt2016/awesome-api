package io.gitee.xjt2016.modules.domain.vo;

import io.gitee.xjt2016.modules.common.config.JqGridTablePage;
import lombok.Data;

@Data
public class UserVo extends JqGridTablePage {
    private String username;
}

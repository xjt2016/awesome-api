package io.gitee.xjt2016.modules.activiti7;

import org.activiti.engine.*;

public class Activiti7 {

    public static final ProcessEngine processEngine;

    static {
        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration
                .createProcessEngineConfigurationFromResource("activiti/activiti.cfg.xml");
        processEngine = processEngineConfiguration.buildProcessEngine();
    }

    public static final RepositoryService repositoryService = processEngine.getRepositoryService();
    public static final RuntimeService runtimeService = processEngine.getRuntimeService();
    public static TaskService taskService = processEngine.getTaskService();

}

package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Field {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long interfaceId;

    @TableField(exist = false)
    private Interface anInterface = new Interface();

    private String name;
    private String dataType;
    private String defaultValue;
    private Integer required = 0;
    private String description;
    private String type;

    private Long pid;

    private Long fid = 0L;
}

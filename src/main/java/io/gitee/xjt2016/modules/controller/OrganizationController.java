package io.gitee.xjt2016.modules.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.sys.Organization;
import io.gitee.xjt2016.modules.domain.vo.ResourceVo;
import io.gitee.xjt2016.modules.service.sys.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/organization")
public class OrganizationController extends BaseController {

    public static final String module = "organization";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    OrganizationService service;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @RequestMapping(value = {"/index", ""})
    public String index() throws Exception {
        return pathPrefix + "/index";
    }

    @ResponseBody
    @GetMapping(value = {"/getListAll"})
    public Object getListAll() throws Exception {
        return buildSuccessResponse(service.list());
    }

    @ResponseBody
    @RequestMapping(value = {"/getList"})
    public Object getList(ResourceVo vo) throws Exception {
        IPage<Organization> page = initMyBatisPage(vo);
        page = service.page(page, Wrappers.lambdaQuery(Organization.class));
        return pageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Organization form = null;
        if (id != null) {
            form = service.getById(id);
        }
        if (form == null) {
            form = new Organization();
        }
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @ResponseBody
    @PostMapping(value = {"/form"})
    public Object formPost(Organization form, RedirectAttributes attributes) throws Exception {
        boolean result = service.saveOrUpdate(form);
        return buildResponse(result);
    }

    @PostMapping(value = "/saveAll")
    @ResponseBody
    public Object saveAll(String params) {
        List<Organization> organizationList = JSON.parseArray(params, Organization.class);
        boolean result = service.saveOrUpdateBatch(organizationList);
        return buildSuccessResponse(result);
    }

    @ResponseBody
    @PostMapping(value = {"/delete"})
    public Object deletePost(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        return buildResponse(result);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public ModelAndView delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

}

package io.gitee.xjt2016.modules.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.Parameter;
import io.gitee.xjt2016.modules.mapper.ParameterMapper;
import org.springframework.stereotype.Service;

@Service
public class ParameterService extends ServiceImpl<ParameterMapper, Parameter> {
}

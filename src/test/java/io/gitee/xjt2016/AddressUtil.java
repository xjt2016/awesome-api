package io.gitee.xjt2016;

import org.apache.commons.lang.StringUtils;

public class AddressUtil {

    public static String resolveName(String firstName, String currentName) {
        String reg = "[0-9a-zA-Z]";
        if (StringUtils.isNotBlank(currentName)) {
            currentName = currentName.replaceAll(reg, currentName);
        }
        firstName = firstName.replaceAll(reg, firstName);
        return firstName;
    }

    public static void main(String[] args) {
        int i = 0;
        while (true) {
            i += 1;
            System.out.println(resolveName(new String("safalsfhlaksf;OAU SD  ASDAS" + i), "S"));

        }
    }
}

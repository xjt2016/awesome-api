package io.gitee.xjt2016.modules.activiti7;

import cn.hutool.core.date.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipInputStream;

@Log4j2
public class Activti7Test extends Activiti7 {
    @Test
    public void test001() {
        //使用classpath下的activiti.cfg.xml中的配置创建processEngine
        log.debug(processEngine);
    }

    //通过bpmn部署流程
    @Test
    public void deploy() {
        String filename = "activiti/processes/leave/leave.bpmn";
        String pngName = "activiti/processes/leave/leave.png";
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource(filename)
                .addClasspathResource(pngName)//图片
                .name("请假流程")
                .deploy();
        log.debug(deployment.getName());
    }

    //通过ZIP部署流程
    @Test
    public void deployByZip() {
        InputStream fileInputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("activiti/processes/leave/leave.zip");
        assert fileInputStream != null;
        ZipInputStream zip = new ZipInputStream(fileInputStream);
        Deployment deployment = repositoryService.createDeployment()
                .addZipInputStream(zip)
                .name("流程部署测试zip")
                .deploy();
        log.debug(deployment.getName());
    }

    //查询流程部署
    @Test
    public void getDeployments() {
        List<Deployment> list = repositoryService.createDeploymentQuery().list();
        for (Deployment dep : list) {
            log.debug("Id：" + dep.getId());
            log.debug("Name：" + dep.getName());
            log.debug("DeploymentTime：" + DateUtil.formatDateTime(dep.getDeploymentTime()));
            log.debug("Key：" + dep.getKey());
            log.debug("------------");
        }

    }

    //查询流程定义
    @Test
    public void getDefinitions() {
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery()
                .list();
        for (ProcessDefinition processDefinition : list) {
            log.debug("------流程定义--------");
            log.debug("Name：" + processDefinition.getName());
            log.debug("Key：" + processDefinition.getKey());
            log.debug("ResourceName：" + processDefinition.getResourceName());
            log.debug("DeploymentId：" + processDefinition.getDeploymentId());
            log.debug("Version：" + processDefinition.getVersion());
        }
    }

    /**
     * 启动流程实例
     */
    @Test
    public void startProcessInstance() {
        String processDefinitionKey = "leave";
        ProcessInstance pInstance = processEngine.getRuntimeService().startProcessInstanceByKey(processDefinitionKey);
        System.out.println("流程实例Id：" + pInstance.getId());
        System.out.println("流程定义Id：" + pInstance.getProcessDefinitionId());
        //流程实例Id：17501
        //流程定义Id：leave:4:15004

        //流程实例Id：20001
        //流程定义Id：leave:4:15004
    }

    //获取流程实例列表
    @Test
    public void getProcessInstances() {
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().list();
        for (ProcessInstance processInstance : list) {
            log.debug("--------流程实例------");
            log.debug("ProcessInstanceId：" + processInstance.getProcessInstanceId());
            log.debug("ProcessDefinitionId：" + processInstance.getProcessDefinitionId());
            log.debug("isEnded" + processInstance.isEnded());
            log.debug("isSuspended：" + processInstance.isSuspended());
        }
    }

    //暂停与激活流程实例
    @Test
    public void suspendAndActiveProcessInstance() {
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().list();

        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        ProcessInstance processInstance = list.get(0);
        String processInstanceId = processInstance.getProcessInstanceId();
        runtimeService.suspendProcessInstanceById(processInstanceId);
        log.debug("挂起流程实例:" + processInstanceId);

        runtimeService.activateProcessInstanceById(processInstanceId);
        log.debug("激活流程实例:" + processInstanceId);
    }

    //删除流程实例
    @Test
    public void delProcessInstance() {
        runtimeService.deleteProcessInstance("73f0fb9a-ce5b-11ea-bf67-dcfb4875e032", "删着玩");
        System.out.println("删除流程实例");
    }


    //删除流程定义
    @Test
    public void delDefinition() {
        String pdID = "44b15cfe-ce3e-11ea-92a3-dcfb4875e032";
        repositoryService.deleteDeployment(pdID, true);
        log.debug("删除流程定义成功");
    }

    private Deployment deployByZip2() {
        InputStream fileInputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("activiti/processes/leave/leave.zip");
        assert fileInputStream != null;
        ZipInputStream zip = new ZipInputStream(fileInputStream);
        return repositoryService.createDeployment()
                .addZipInputStream(zip)
                .name("流程部署测试zip")
                .deploy();
    }

    @Test
    public void testAll() {

        //部署
        Deployment deployment = deployByZip2();
        log.debug("发布流程：" + deployment.getId());
        String deploymentId = deployment.getId();

        //查询
        Optional<Deployment> deploymentOptional = getDeploymentById(deploymentId);
        log.debug("查询流程{}，结果:{}", deployment.getId(), deploymentOptional.isPresent());


        //1、获取页面表单填报的内容，请假时间，请假事由，String fromData
        //2、fromData 写入业务表，返回业务表主键ID==businessKey
        //3、把业务数据与Activiti7流程数据关联
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(deploymentId);
        log.debug("流程实例ID：" + processInstance.getProcessDefinitionId());


        //删除
        //repositoryService.deleteDeployment(deploymentId);
        log.debug("删除流程{}，结果:{}", deployment.getId(), !getDeploymentById(deploymentId).isPresent());


    }

    public Optional<Deployment> getDeploymentById(String id) {
        List<Deployment> deployments = repositoryService.createDeploymentQuery().deploymentId(id).list();
        if (CollectionUtils.isNotEmpty(deployments)) {
            return Optional.ofNullable(deployments.get(0));
        }
        return Optional.empty();
    }

//    @Test
//    public void test003() {
//        ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml")
//                .buildProcessEngine();
//       log.debug("processEngine:"+processEngine);
//    }

    /**
     * 使用代码创建工作流需要的23张表
     */
//    @Test
//    public void createTable() {
//        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration
//                .createStandaloneProcessEngineConfiguration();
//        //连接数据库的配置
//        //配置数据库驱动:对应不同数据库类型的驱动
//        processEngineConfiguration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
//        //配置数据库的JDBC URL
//        processEngineConfiguration.setJdbcUrl("jdbc:mysql://www.xjt.pub:3306/awesome-api?useUnicode=true;characterEncoding=utf-8;useSSL=false");
//        //配置连接数据库的用户名
//        processEngineConfiguration.setJdbcUsername("root");
//        //配置连接数据库的密码
//        processEngineConfiguration.setJdbcPassword("OdmC6CsQn3IHkbSi");
//        /**
//         public static final String DB_SCHEMA_UPDATE_FALSE = "false";不能自动创建表，需要表存在
//         public static final String DB_SCHEMA_UPDATE_CREATE_DROP = "create-drop";先删除表再创建表
//         public static final String DB_SCHEMA_UPDATE_TRUE = "true";如果表不存在，自动创建表
//         */
//        processEngineConfiguration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
//        //工作流的核心对象，ProcessEnginee对象
//        ProcessEngine processEngine = processEngineConfiguration.buildProcessEngine();
//       log.debug("processEngine:" + processEngine);
//    }
}

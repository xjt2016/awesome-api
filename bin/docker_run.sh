#!/bin/bash
export CURRENT_PATH=$(cd `dirname $0`; pwd)
export PROJECT_ROOT=$(cd `dirname ${CURRENT_PATH}`; pwd)
export PROJECT_NAME=awesome-api
export FILE_NAME='awesome-api.jar'
export CODE_PATH=${PROJECT_ROOT}/target/code
export JAVA_OPTS=" -Xms128m -Xmx1g -XX:+UseParNewGC -XX:+UseConcMarkSweepGC "

echo '当前项目目录：' ${PROJECT_ROOT}

echo '执行maven命令：cd ${PROJECT_ROOT} && mvn clean package -DskipTests=true'
cd ${PROJECT_ROOT} && mvn clean package -DskipTests=true

echo "unzip ${FILE_NAME} -d ${CODE_PATH}"
rm -rf ${CODE_PATH} && unzip ${PROJECT_ROOT}/target/${FILE_NAME} -d ${CODE_PATH}

cd ${PROJECT_ROOT}/docker && docker-compose stop && docker-compose up -d --build
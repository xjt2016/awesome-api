package io.gitee.xjt2016.modules.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.Resource;
import io.gitee.xjt2016.modules.domain.sys.ResourceType;
import io.gitee.xjt2016.modules.gen.utils.GenUtil;
import io.gitee.xjt2016.modules.service.ResourceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = {"/", "/index"})
    public String index() throws Exception {
        return "index";
    }

    @RequestMapping(value = {"/treeSelect",})
    public String treeSelect(String url, Model model) throws Exception {
        model.addAttribute("url", url);
        return "common/treeSelect";
    }

    @GetMapping(value = "/treeMultiSelect")
    public String treeMultiSelect(HttpServletRequest request, String url, String selectIds) {
        request.setAttribute("url", url);
        request.setAttribute("selectIds", selectIds);
        return "/common/treeMultiSelect";
    }

    @javax.annotation.Resource
    ResourceService resourceService;

    @ResponseBody
    @RequestMapping(value = {"/getMenuList"})
    public Object getMenuList() throws Exception {
        List<Resource> menuList = resourceService.list(Wrappers.lambdaQuery(Resource.class)
                .eq(Resource::getAvailable, 1)
                .eq(Resource::getType, ResourceType.menu)
        );

//        Map<Long, List<Menu>> menuListMap = menuList.stream()
//                .collect(Collectors.groupingBy(Menu::getPid));
//
//        Map<Long, Menu> menuMap = menuList.stream()
//                .collect(Collectors.toMap(Menu::getPid, Function.identity(), (menu1, menu2) -> menu1));
        List<Resource> menus = listToTree2(menuList, Resource.ROOT_ID);


        Map<String, Object> map = new HashMap<>();
        menus = menus.stream()
                .filter(resource -> Objects.equals(resource.getPid(), Resource.ROOT_ID))
                .flatMap((Function<Resource, Stream<Resource>>) resource -> resource.getChildren().stream())
                .collect(Collectors.toList());
        map.put("menus", menus);

        String html = GenUtil.render(map, "gen/templates/html/menu.html");
        return buildSuccessResponse(html);
    }

    public List<Resource> listToTree2(List<Resource> list, Object root) {
        List<Resource> tree = new ArrayList<>();
        for (Resource user : list) {
            //找到根节点
            if (user.getPid() == null || Objects.equals(root, user.getPid())) {
                tree.add(findChildren(user, list));
            }
        }
        return tree;
    }

    private Resource findChildren(Resource user, List<Resource> list) {
        List<Resource> children = new ArrayList<>();
        for (Resource node : list) {
            if (node.getPid().equals(user.getId())) {
                //递归调用
                children.add(findChildren(node, list));
            }
        }
        user.setChildren(children);
        return user;
    }

}

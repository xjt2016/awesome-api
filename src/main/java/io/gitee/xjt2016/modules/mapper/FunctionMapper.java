package io.gitee.xjt2016.modules.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.Function;

public interface FunctionMapper extends BaseMapper<Function> {
}

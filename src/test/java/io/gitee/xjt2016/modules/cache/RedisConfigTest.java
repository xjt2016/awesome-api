package io.gitee.xjt2016.modules.cache;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Before;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RBoundedBlockingQueue;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.HyperLogLogOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class RedisConfigTest {
    //spring.redis.host=www.xjt.pub
//spring.redis.port=16379
//spring.redis.password=OdmC6CsQn3IHkbSi
    RedisTemplate<String, Object> redisTemplate;

    RedissonClient redisson;

    static {
        Configurator.setAllLevels("", Level.ERROR);
    }

    @Before
    public void before() {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName("www.xjt.pub");
        configuration.setPort(16379);
        configuration.setPassword("OdmC6CsQn3IHkbSi");
        LettuceConnectionFactory factory = new LettuceConnectionFactory(configuration);
        factory.afterPropertiesSet();
        RedisConfig redisConfig = new RedisConfig();
        redisTemplate = redisConfig.redisTemplate(factory);

        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://www.xjt.pub:16379")
                .setPassword("OdmC6CsQn3IHkbSi")
        ;

        redisson = Redisson.create(config);
    }

    @Test
    public void string() {
        redisTemplate.opsForValue().set("name", "tom");
        System.out.println(redisTemplate.opsForValue().get("name"));
    }

    @Test
    public void setIfAbsent() {
        System.out.println(redisTemplate.opsForValue().setIfAbsent("multi1", "multi1", 10, TimeUnit.SECONDS));//false  multi1之前已经存在
        System.out.println(redisTemplate.opsForValue().setIfAbsent("multi1", "multi12", 10, TimeUnit.SECONDS));//true  multi111之前不存在
    }

    @Test
    public void multiSet() {
        Map<String, String> maps = new HashMap<>();
        maps.put("multi1", "multi1");
        maps.put("multi2", "multi2");
        maps.put("multi3", "multi3");
        redisTemplate.opsForValue().multiSet(maps);
        List<String> keys = new ArrayList<>();
        keys.add("multi1");
        keys.add("multi2");
        keys.add("multi3");
        System.out.println(redisTemplate.opsForValue().multiGet(keys));
    }

    @Test
    public void increment() {
        String key = "increlong";
        redisTemplate.opsForValue().increment(key, 1);
        System.out.println(redisTemplate.opsForValue().get(key));
    }


    @Test
    public void distributeId() {
        RedisSequenceFactory factory = new RedisSequenceFactory(redisTemplate);
        String key = "distributeId";
        System.out.println(factory.generate(key, RedisSequenceFactory.getTodayEndTime()));
        System.out.println(factory.generateOrderId());
    }

    @Test
    public void testHyperLogLog() {
        String key = "testHyperLogLog";
        HyperLogLogOperations<String, Object> operations = redisTemplate.opsForHyperLogLog();
        for (int i = 0; i < 10; i++) {
            Long result = redisTemplate.opsForHyperLogLog().add(key, RandomUtil.randomInt(30));
//            System.out.println(result);
        }
        System.out.println(operations.size(key));
    }


    @Test
    public void testBitmap() {

    }

    @Test
    public void testLua() {
    }

    @Test
    public void testQueue() throws InterruptedException, IOException {
        String template_Queue = "list:";
        ListOperations<String, Object> operation = redisTemplate.opsForList();
        AtomicInteger integer = new AtomicInteger();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        int num = integer.incrementAndGet();

                        operation.leftPush(template_Queue, num + "");
                        TimeUnit.SECONDS.sleep(RandomUtil.randomInt(3));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, "producer" + i).start();
        }

        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        Object data = operation.rightPop(template_Queue, 60, TimeUnit.SECONDS);
                        System.out.println(data + "," + operation.size(template_Queue));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }, "consumer-" + i).start();
        }
        System.in.read();

    }

    @Test
    public void testQueue2() throws InterruptedException, IOException {
        String template_Queue = "list:";
        RBoundedBlockingQueue<String> queue = redisson.getBoundedBlockingQueue("anyQueue");
        queue.trySetCapacity(100); //尝试设置阻塞队列的容量,trySetCapacity() 返回值： boolean

        AtomicInteger integer = new AtomicInteger();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        int num = integer.incrementAndGet();
                        queue.offer(num + "", 60, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, "producer" + i).start();
        }

        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        System.out.println(queue.poll(10, TimeUnit.SECONDS) + "," + queue.size());
                        TimeUnit.SECONDS.sleep(RandomUtil.randomInt(5));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }, "consumer-" + i).start();
        }
        System.in.read();

    }
}
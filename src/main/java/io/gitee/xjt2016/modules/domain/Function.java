package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@TableName(value = "`function`")
public class Function {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long projectId;

    @TableField(exist = false)
    private Project project = new Project();

    private String name;
    private String description;

    @TableField(exist = false)
    private List<Interface> interfaceList = new ArrayList<>();
}

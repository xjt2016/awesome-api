package io.gitee.xjt2016.modules.common;

import jetbrick.io.resource.ClasspathResource;
import jetbrick.io.resource.Resource;
import jetbrick.template.loader.AbstractResourceLoader;
import jetbrick.util.PathUtils;

public class MyResourceLoader extends AbstractResourceLoader {
    public MyResourceLoader() {
        root = "";
        reloadable = true;
    }

    @Override
    public Resource load(String name) {
        String path = PathUtils.concat(root, name);
        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        ClasspathResource resource = new ClasspathResource(path);
        if (!resource.exist()) {
            return null;
        }

        resource.setRelativePathName(name); // use relative name
        return resource;
    }

}

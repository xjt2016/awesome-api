package io.gitee.xjt2016.modules.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Project {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private String description;
}

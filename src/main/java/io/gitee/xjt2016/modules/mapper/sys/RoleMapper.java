package io.gitee.xjt2016.modules.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.sys.Permission;
import io.gitee.xjt2016.modules.domain.sys.Role;

public interface RoleMapper extends BaseMapper<Role> {
}

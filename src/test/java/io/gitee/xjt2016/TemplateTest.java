package io.gitee.xjt2016;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import io.gitee.xjt2016.modules.domain.Resource;
import io.gitee.xjt2016.modules.gen.utils.GenUtil;
import org.junit.Test;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TemplateTest {

    @Test
    public void testMenu() {

        String list = "[{\"id\":1,\"name\":\"项目\",\"url\":\"/project\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":3,\"name\":\"功能\",\"url\":\"/function\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":4,\"name\":\"接口\",\"url\":\"/interface\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":5,\"name\":\"字段\",\"url\":\"/field\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":6,\"name\":\"参数\",\"url\":\"/parameter\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":8,\"name\":\"代码生成\",\"url\":\"/gen\",\"enabled\":1,\"pid\":0,\"children\":[],\"root\":false},{\"id\":11,\"name\":\"格式化\",\"url\":\"/format\",\"enabled\":1,\"pid\":0,\"children\":[{\"id\":9,\"name\":\"SQL格式化\",\"url\":\"/sql/format\",\"enabled\":1,\"pid\":11,\"children\":[],\"root\":false},{\"id\":10,\"name\":\"JSON格式化\",\"url\":\"/format/json\",\"enabled\":1,\"pid\":11,\"children\":[],\"root\":false}],\"root\":false}]";
        List<Resource> menus = JSON.parseArray(list, Resource.class);
        Map<String, Object> map = new HashMap<>();
        map.put("menus", menus);
        String html = GenUtil.render(map, "gen/templates/html/menu.html");
        System.out.println(html);
    }

    @Test
    public void loopJson() {
        String list = "{\"Array\":[1,2,\"3\"],\"Boolean\":true,\"Null\":null,\"Number\":123,\"Object\":{\"a\":\"b\",\"c\":\"d\"},\"String\":\"Hello World\"}\n";
        Object obj = JSON.parse(list);

        JSONObject root = null;
        if (obj instanceof JSONObject) {
            root = (JSONObject) obj;
        } else if (obj instanceof JSONArray) {
            JSONArray jsonArray = ((JSONArray) obj);
            if (jsonArray.size() > 0) {
                root = (JSONObject) ((JSONArray) obj).get(0);
            }
        }
        Preconditions.checkArgument(root != null && root.keySet().size() > 0);

        Map<String, List<FieldInfo>> map = new HashMap<>();

        map.put("root", new ArrayList<>());
        for (Map.Entry<String, Object> entry : root.entrySet()) {
            String type = entry.getValue().getClass().getTypeName();
            if (entry.getValue() instanceof JSONArray) {

            } else if (entry.getValue() instanceof JSONObject) {
                type = entry.getKey();
            } else {

            }
            map.get("root").add(new FieldInfo(entry.getKey(), type));
        }
    }

    public List<FieldInfo> analyze(JSONObject obj, Map<String, List<FieldInfo>> map) {
        return obj.entrySet().stream().distinct().map(entry -> {
            String type = entry.getValue().getClass().getTypeName();
            if (entry.getValue() instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) entry.getValue();
                for (Object jsonObj : jsonArray) {
                }
            } else if (entry.getValue() instanceof JSONObject) {
                type = entry.getKey();
                map.putIfAbsent(type, new ArrayList<>());
                analyze((JSONObject) entry.getValue(), map);
            } else {

            }
            return new FieldInfo(entry.getKey(), type);
        }).collect(Collectors.toList());
    }

    @Test
    public void testSocket() throws IOException {
        String address = "122.112.163.221";
        int port = 9998;

        //客户端
        //1、创建客户端Socket，指定服务器地址和端口
        Socket socket = new Socket(address, port);
        //2、获取输出流，向服务器端发送信息
        OutputStream os = socket.getOutputStream();//字节输出流
        PrintWriter pw = new PrintWriter(os);//将输出流包装成打印流
        pw.write("用户名：admin；密码：123");
        pw.flush();
        socket.shutdownOutput();
        //3、获取输入流，并读取服务器端的响应信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info;
        while ((info = br.readLine()) != null) {
            System.out.println("我是客户端，服务器说：" + info);
        }

        //4、关闭资源
        br.close();
        is.close();
        pw.close();
        os.close();
        socket.close();
    }

    @Test
    public void test003() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("truncate table multi_order_" + i + ";");
            System.out.println("truncate table multi_trade_" + i + ";");
        }
    }
}

package io.gitee.xjt2016.modules.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.Field;
import io.gitee.xjt2016.modules.domain.FieldDataType;
import io.gitee.xjt2016.modules.domain.FieldType;
import io.gitee.xjt2016.modules.domain.vo.FieldVo;
import io.gitee.xjt2016.modules.service.FieldService;
import io.gitee.xjt2016.modules.service.FunctionService;
import io.gitee.xjt2016.modules.service.InterfaceService;
import io.gitee.xjt2016.modules.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/field")
public class FieldController extends BaseController {

    public static final String module = "field";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    FieldService service;

    @Autowired
    FunctionService functionService;

    @Autowired
    ProjectService projectService;

    @Autowired
    InterfaceService interfaceService;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @RequestMapping(value = {"/index", ""})
    public String index() throws Exception {
        return pathPrefix + "/index";
    }

    @ResponseBody
    @RequestMapping(value = {"/getList"})
    public Object getList(FieldVo vo) throws Exception {
        IPage<Field> page = initMyBatisPage(vo);
        page = service.page(page, Wrappers.lambdaQuery(Field.class));

        page.getRecords().forEach(anField -> {
            anField.setAnInterface(interfaceService.getById(anField.getInterfaceId()));
            if (anField.getAnInterface() != null) {
                anField.getAnInterface().setFunction(functionService.getFunction(anField.getAnInterface().getFunctionId()));
            }
        });
        return pageResponse(page);
    }

    @ResponseBody
    @RequestMapping(value = {"/getFieldDataType"})
    public Object getFieldDataType(FieldVo vo) throws Exception {
        List<Map<String, Object>> list = Arrays.stream(FieldDataType.values()).map(BeanUtil::beanToMap).collect(Collectors.toList());
        return buildSuccessResponse(list);
    }

    @ResponseBody
    @RequestMapping(value = {"/getFieldType"})
    public Object getFieldType(FieldVo vo) throws Exception {
        List<Map<String, Object>> list = Arrays.stream(FieldType.values()).map(BeanUtil::beanToMap).collect(Collectors.toList());
        return buildSuccessResponse(list);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Field form = service.getField(id);
        if (form == null) {
            form = new Field();
        }
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @PostMapping(value = {"/form"})
    public ModelAndView formPost(Field form, RedirectAttributes attributes) throws Exception {
        boolean result = service.saveOrUpdate(form);
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public ModelAndView delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

}

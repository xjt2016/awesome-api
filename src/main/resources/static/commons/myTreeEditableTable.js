function initTable(_tableData, _tableParams) {
    var $table = $('#' + _tableParams.tableId);
    var _columns = _tableParams.tableColumns
    var options = {
        columns: _columns,
        data: _tableData,
        //在哪一列展开树形
        treeShowField: 'name',
        //指定父id列
        parentIdField: 'pid',

        onResetView: function (data) {
            $table.treegrid({
                initialState: 'expanded',// 所有节点都折叠
                // initialState: 'expanded',// 所有节点都展开，默认展开
                treeColumn: 1,
                expanderExpandedClass: 'glyphicon glyphicon-minus',  //图标样式
                expanderCollapsedClass: 'glyphicon glyphicon-plus',
                onChange: function () {
                    $table.bootstrapTable('resetWidth');
                }
            });

            //只展开树形的第一级节点
            $table.treegrid('getRootNodes').treegrid('expand');

        },
        onCheck: function (row) {
            var datas = $table.bootstrapTable('getData');
            // 勾选子类
            selectChilds(datas, row, "id", "pid", true);
            // 勾选父类
            selectParentChecked(datas, row, "id", "pid")
            // 刷新数据
            $table.bootstrapTable('load', datas);
        },

        onUncheck: function (row) {
            var datas = $table.bootstrapTable('getData');
            selectChilds(datas, row, "id", "pid", false);
            $table.bootstrapTable('load', datas);
        },
        onEditableSave: function (field, row, oldValue, $el) {
            var newRow = newTableRow();
            for (key in newRow) {
                for (field in row) {
                    if (key === field) {
                        newRow[key] = row[field];
                    }
                }
            }
            // console.log(newRow);
            // console.log(JSON.stringify(newRow));

            $.ajax({
                type: "post",
                url: _tableParams.saveRowAjaxUrl,
                data: newRow,
                dataType: 'JSON',
                success: function (data, status) {
                    if (data.result && data.result == 100) {
                        toastr.success('更新成功')
                    } else {
                        toastr.error('更新失败：' + result.message);
                    }
                    initTableByAjax(tableParams);
                },
                error: function () {
                    toastr.error('更新失败')
                },
            });
        }

        // bootstrap-table-treetreegrid.js 插件配置 -- end
    };
    $table.bootstrapTable('destroy');
    $table.bootstrapTable(options);
}

function delRowByBtn(obj, delUrl) {
    confirmx('确认删除吗?', function () {
        var _obj = $(obj);
        var id = _obj.attr('data-id');
        var _table = _obj.parents('table').eq(0);

        _table.bootstrapTable('remove', {
            field: 'id',
            values: [parseInt(id)]
        });
        toastr.success('删除成功');
    });
}

function addRowByBtn(obj) {
    var _obj = $(obj);
    var id = _obj.attr('data-id');
    var _table = _obj.parents('table').eq(0);
    addRow(_table, id);
    toastr.success('添加成功');
}

function addRow(_table, id) {
    if (id) {
        id = parseInt(id);
    }
    var _data = _table.bootstrapTable('getData');
    var row = {};
    _data.forEach(function (_item) {
        if (_item.id === id) {
            row = _item;
        }
    });
    var newRow = initRow(row.id)
    _table.bootstrapTable('append', newRow);
    console.log('add', id, row, newRow);
    toastr.success('添加成功');
}

function initRow(pid) {
    if (!pid) {
        pid = 0;
    }
    var timestamp = Date.parse(new Date());
    return {
        id: timestamp,
        pid: parseInt(pid),
        name: 'Item 1',
        dataType: 'String',
        defaultValue: 'String',
        required: 0,
        type: 'REQUEST',
    };
}

function addRowByTableId(tableId) {
    var _table = $('#' + tableId);
    var rootId = 1;
    addRow(_table, rootId);
}

function saveRowsByTableId(tableId, buildParams, executeSave) {
    executeSave(buildParams(_data));
}

function initTableByAjax(tableParams) {
    var getDataAjaxUrl = tableParams.getDataAjaxUrl;
    $.get(getDataAjaxUrl, function (data, status) {
        if (data && data.result === 100) {
            var tableData = data.data;
            initTable(tableData, tableParams);
            // //tableId 表格对象的ID
            // var options = $("#" + tableId).bootstrapTable('getOptions');
            // //获取每页显示多少条数据
            // var pageSize = options.pageSize;
            // //获取表格的列对象
            // var _columns = options.columns;
            // console.log(_columns);

            $('.btn-delRow').on('click', function () {
                var id = $(this).attr('data-id');
                // var _table = _obj.parents('table').eq(0);
                $.ajax({
                    type: 'POST',
                    url: tableParams.delRowAjaxUrl,
                    data: {id: id},
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            if (result.result && result.result == 100) {
                                toastr.success('保存成功');
                            } else {
                                toastr.error('保存失败：' + result.message);
                            }
                        } else {
                            toastr.error('保存失败：');
                        }
                        initTableByAjax(tableParams);
                    },
                });
            });

            $('.btn-addRow').on('click', function () {
                var _obj = $(this);
                var id = _obj.attr('data-id');
                var _table = _obj.parents('table').eq(0);

                if (id) {
                    id = parseInt(id);
                }
                var _data = _table.bootstrapTable('getData');
                var row = {};
                _data.forEach(function (_item) {
                    if (_item.id === id) {
                        row = _item;
                    }
                });
                var newRow = newTableRow();
                newRow.pid = !row.id ? 0 : row.id;
                $.ajax({
                    type: 'POST',
                    url: tableParams.addRowAjaxUrl,
                    data: newRow,
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            if (result.result && result.result == 100) {
                                _table.bootstrapTable('append', newRow);
                                toastr.success('添加成功');
                            } else {
                                toastr.error('添加失败：' + result.message);
                            }
                        } else {
                            toastr.error('添加失败：');
                        }
                        initTableByAjax(tableParams);
                    },
                });
            });

        }
    });

    if ($('#treeTableSaveBtn')) {
        $('#treeTableSaveBtn').on('click', function () {
            var _data = $('#' + tableParams.tableId).bootstrapTable('getData');
            var params = [];
            if (_data) {
                _data.forEach(function (item) {
                    item = {
                        id: item.id,
                        name: item.name,
                        pid: item.pid,
                        available: item.available,
                    }
                    params.push(item);
                });
            }
            $.ajax({
                type: 'POST',
                url: tableParams.saveTableDataAjaxUrl,
                data: {params: JSON.stringify(params)},
                dataType: 'json',
                success: function (result) {
                    if (result) {
                        if (result.result && result.result == 100) {
                            toastr.success('保存成功');
                            $('#' + _tableId).bootstrapTable('refresh');
                        } else {
                            toastr.error('保存失败：' + result.message);
                        }
                    } else {
                        toastr.error('保存失败：');
                    }
                },
            });
        });
    }
    if ($('#refreshTableBtn')) {
        $('#refreshTableBtn').on('click', function () {
            initTableByAjax(tableParams);
            toastr.success('刷新成功');
        });
    }

}
package io.gitee.xjt2016.modules.basic;

import com.alibaba.fastjson.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class HandleString {

    public int json2prop(JSONObject jsonObject, String tmp_key, Map<String, Object> tmp_config) {
        Iterator<String> it = jsonObject.keySet().iterator();
        StringBuilder tmp_keyBuilder = new StringBuilder(tmp_key);
        while (it.hasNext()) {
            // 获得key
            String key = it.next();
            String value = jsonObject.getString(key);
            try {
                JSONObject jsonStr = JSONObject.parseObject(value);
                String tmp_key_pre = tmp_keyBuilder.toString();
                tmp_keyBuilder.append(key).append(".");
                this.json2prop(jsonStr, tmp_keyBuilder.toString(), tmp_config);
                tmp_keyBuilder = new StringBuilder(tmp_key_pre);

            } catch (Exception e) {
                tmp_config.put(tmp_keyBuilder + key, value);
                System.out.println(tmp_keyBuilder + key + "=" + value);
                continue;
            }
        }
        tmp_key = tmp_keyBuilder.toString();
        return 0;
    }
}
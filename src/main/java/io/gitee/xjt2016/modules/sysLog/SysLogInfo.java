package io.gitee.xjt2016.modules.sysLog;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class SysLogInfo {

    private String operation;
    private String method;
    private String params;
    private String ip;
    private String username;
    private long time;
    private Date createDate;
}

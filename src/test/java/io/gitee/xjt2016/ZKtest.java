package io.gitee.xjt2016;

import org.apache.commons.io.FileUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ZKtest {
    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {

        ZooKeeper zooKeeper = new ZooKeeper("zoo1.superboss.cc:30002", 5000, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                System.out.println("evnent:" + watchedEvent.toString());
            }
        });

        List<String> list = zooKeeper.getChildren("/dubbo", false);
        for (String api : list) {
            String str = api + "\tproviders:" + zooKeeper.getChildren("/dubbo/" + api + "/providers", false).size() + "\tconsumers:" + zooKeeper.getChildren("/dubbo/" + api + "/consumers", false).size()+"\n";
            FileUtils.write(new File("dubbo.txt1"), str, "utf-8", true);
            System.out.println(api);
        }
        zooKeeper.close();
    }
}
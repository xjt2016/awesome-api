#!/bin/bash
export CURRENT_PATH=$(cd `dirname $0`; pwd)
export PROJECT_ROOT=$(cd `dirname ${CURRENT_PATH}`; pwd)
export PROJECT_NAME=awesome-api
export FILE_NAME='awesome-api.jar'
export CODE_PATH=${PROJECT_ROOT}/target/code
export JAVA_OPTS=" -Xms1g -Xmx2g -XX:+UseParNewGC -XX:+UseConcMarkSweepGC "

echo '当前项目目录：' ${PROJECT_ROOT}

if [ ! -f "${PROJECT_ROOT}/target/${FILE_NAME}" ];then
  echo '执行maven命令：mvn clean package -DskipTests=true'
  cd ${PROJECT_ROOT} && mvn clean package -DskipTests=true
else
  echo '已存在，不构建:' ${PROJECT_ROOT}/${FILE_NAME}
fi

echo "unzip ${FILE_NAME} -d ${CODE_PATH}"
rm -rf ${CODE_PATH} && unzip ${PROJECT_ROOT}/target/${FILE_NAME} -d ${CODE_PATH}

cd ${CODE_PATH} && java ${JAVA_OPTS} -Dname=${PROJECT_NAME} org.springframework.boot.loader.JarLauncher

#cd ${CODE_PATH} && nohup java  ${JAVA_OPTS} -Dname=${PROJECT_NAME} org.springframework.boot.loader.JarLauncher >/dev/null 2>&1 &

package io.gitee.xjt2016.modules.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.Parameter;
import io.gitee.xjt2016.modules.domain.vo.ParameterVo;
import io.gitee.xjt2016.modules.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/parameter")
public class ParameterController extends BaseController {

    public static final String module = "parameter";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    ParameterService service;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @RequestMapping(value = {"/index", ""})
    public String index() throws Exception {
        return pathPrefix + "/index";
    }

    @ResponseBody
    @RequestMapping(value = {"/getList"})
    public Object getList(ParameterVo vo) throws Exception {
        IPage<Parameter> page = initMyBatisPage(vo);
        page = service.page(page, Wrappers.lambdaQuery(Parameter.class));
        return pageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Parameter form = service.getById(id);
        if (form == null) {
            form = new Parameter();
        }
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @PostMapping(value = {"/form"})
    public ModelAndView formPost(Parameter form, RedirectAttributes attributes) throws Exception {
        boolean result = service.saveOrUpdate(form);
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public ModelAndView delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

}

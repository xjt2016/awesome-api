package io.gitee.xjt2016.modules.domain;

import lombok.Data;

@Data
public class ApiSendInfo {
    private String url;
    private int status;
    private String remoteIpAddress;
    private String date;
    private long totalResponseTime;
    private int requestBodySize;
    private int responseBodySize;
}

package io.gitee.xjt2016.modules.domain;

import lombok.Data;

import java.util.Map;

@Data
public class ApiSendRequest {
    private String method;
    private String url;
    private Map<String, String> headers;
    private Map<String, Object> params;
    private String body;
}

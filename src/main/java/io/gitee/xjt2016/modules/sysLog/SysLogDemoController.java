package io.gitee.xjt2016.modules.sysLog;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys")
public class SysLogDemoController {

    @SysLog(value = "sysLog")
    @GetMapping(value = {"/log",})
    public String index() throws Exception {
        return "index";
    }

}

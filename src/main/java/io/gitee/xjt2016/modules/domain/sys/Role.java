package io.gitee.xjt2016.modules.domain.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "sys_role")
public class Role implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name; //角色标识 程序中判断使用,如"admin"
    private String description; //角色描述,UI界面显示使用

    private String resourceIds = ""; //拥有的资源

    @TableField(exist = false)
    private String resourceNames = ""; //拥有的资源
    private Integer available = 0; //是否可用,如果不可用将不会添加给用户

    private Date created;
    private Date modified;

    public Role() {
    }

    public Role(String name, String description, Integer available) {
        this.name = name;
        this.description = description;
        this.available = available;
    }
}

#!/bin/bash

#启动
PROJECT_NAME=awesome-api
FILE_NAME=awesome-api.jar

JAVA_OPTS="-Xms128m -Xmx1G -XX:+UseParNewGC -XX:+UseConcMarkSweepGC "

CURRENT_PATH=$(cd $(dirname $0) || exit; pwd)
PROJECT_ROOT=$(cd $(dirname ${CURRENT_PATH}) || exit; pwd)
echo "${PROJECT_ROOT}/target "
#./nohup.out /dev/null
#-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
cd ${PROJECT_ROOT}/target && nohup java  -jar ${JAVA_OPTS} -Dname=${PROJECT_NAME} ./${FILE_NAME} --spring.profiles.active=prod >/dev/null 2>&1 &

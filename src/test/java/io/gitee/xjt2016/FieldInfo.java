package io.gitee.xjt2016;

import lombok.Data;

@Data
public class FieldInfo {
    private String fieldName;
    private String fieldType;

    public FieldInfo(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }
}
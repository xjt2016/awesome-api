#!/bin/bash
CURRENT_PATH=$(cd `dirname $0`; pwd)
PROJECT_ROOT=$(cd `dirname ${CURRENT_PATH}`; pwd)
cd ${PROJECT_ROOT} && mvn clean package -DskipTests=true && ./bin/shutdown.sh && ./bin/startup.sh

package io.gitee.xjt2016.modules.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.Project;
import io.gitee.xjt2016.modules.domain.vo.ProjectVo;
import io.gitee.xjt2016.modules.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/project")
public class ProjectController extends BaseController {

    public static final String module = "project";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    ProjectService service;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @GetMapping
    public String index() throws Exception {
        return pathPrefix + "/list";
    }

    @ResponseBody
    @RequestMapping(value = {"/treeData", ""})
    public Object treeData() throws Exception {
        return service.list();
    }

    @ResponseBody
    @GetMapping(value = {"/getListAll"})
    public Object getListAll() throws Exception {
        return buildSuccessResponse(service.list());
    }

    @ResponseBody
    @GetMapping(value = {"/getList"})
    public Object getList(ProjectVo vo) throws Exception {
        IPage<Project> page = initMyBatisPage(vo);
        page = service.page(page, Wrappers.lambdaQuery(Project.class));
        return pageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Project form = service.getById(id);
        if (form == null) {
            form = new Project();
        }
        model.addAttribute(PARAM_KEY, form);
        return "/project/form";
    }

    @PostMapping(value = {"/form"})
    public ModelAndView formPost(Project form, RedirectAttributes attributes) throws Exception {
        boolean result = service.saveOrUpdate(form);
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public ModelAndView delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

}

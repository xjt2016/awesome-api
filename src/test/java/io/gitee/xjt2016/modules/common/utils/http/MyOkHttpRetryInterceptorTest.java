package io.gitee.xjt2016.modules.common.utils.http;

import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.junit.Test;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MyOkHttpRetryInterceptorTest {

    @Test
    public void test001() {
        MyOkHttpRetryInterceptor myOkHttpRetryInterceptor = new MyOkHttpRetryInterceptor.Builder()
                .executionCount(3)
                .retryInterval(1000)
                .build();
        new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .addInterceptor(myOkHttpRetryInterceptor)
                .connectionPool(new ConnectionPool())
                .connectTimeout(3000, TimeUnit.MILLISECONDS)
                .readTimeout(10000, TimeUnit.MILLISECONDS)
                .build();
    }


    @Test
    public void test002() {
        //定义重试机制
        Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
                .retryIfException()    //设置异常重试
                .retryIfResult(aBoolean -> Objects.equals(aBoolean, true))    //call方法返回true重试
                .withWaitStrategy(WaitStrategies.fixedWait(10, TimeUnit.SECONDS))   //设置10秒后重试
                .withStopStrategy(StopStrategies.stopAfterAttempt(3)).build(); //设置重试次数 超过将出异常


    }

    @Test
    public void test003() {
//        Retryer<TaskResult> getExecutionStatusRetryer = RetryerBuilder.<TaskResult>newBuilder()
//                .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
//                .retryIfResult(r -> !Constants.completeStatus.contains(r.getStatus()))
//                .withStopStrategy(StopStrategies.neverStop())
//                .build();
    }
}
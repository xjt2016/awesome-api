package io.gitee.xjt2016.modules.domain;

import lombok.Getter;

@Getter
public enum FieldType {
    REQUEST("请求", "REQUEST"), RESPONSE("响应", "RESPONSE");

    private final String name;
    private final String value;

    FieldType(String name, String value) {
        this.name = name;
        this.value = value;
    }

}

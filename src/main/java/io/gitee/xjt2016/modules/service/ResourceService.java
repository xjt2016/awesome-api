package io.gitee.xjt2016.modules.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.Resource;
import io.gitee.xjt2016.modules.mapper.ResourceMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ResourceService extends ServiceImpl<ResourceMapper, Resource> {
    public Set<String> findPermissions(Set<Long> resourceIds) {
        Set<String> permissions = new HashSet<String>();
        for (Long resourceId : resourceIds) {
            Resource resource = getById(resourceId);
            if (resource != null && !StringUtils.isEmpty(resource.getPermission())) {
                permissions.add(resource.getPermission());
            }
        }
        return permissions;
    }

    public String getResourceNameByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return "";
        }
        return this.listByIds(ids).stream()
                .map(Resource::getName)
                .collect(Collectors.joining(","));
    }
}

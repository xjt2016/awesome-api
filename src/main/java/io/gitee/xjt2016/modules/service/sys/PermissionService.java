package io.gitee.xjt2016.modules.service.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.sys.Permission;
import io.gitee.xjt2016.modules.mapper.sys.PermissionMapper;
import org.springframework.stereotype.Service;

@Service
public class PermissionService extends ServiceImpl<PermissionMapper, Permission> {
}

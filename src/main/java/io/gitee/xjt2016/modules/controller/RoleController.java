package io.gitee.xjt2016.modules.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.common.utils.SplitUtil;
import io.gitee.xjt2016.modules.domain.sys.Role;
import io.gitee.xjt2016.modules.domain.vo.RoleVo;
import io.gitee.xjt2016.modules.service.ResourceService;
import io.gitee.xjt2016.modules.service.sys.RoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/role")
public class RoleController extends BaseController {

    public static final String module = "role";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    RoleService service;

    @Resource
    ResourceService resourceService;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @GetMapping
    public String index() throws Exception {
        return pathPrefix + "/list";
    }

    @ResponseBody
    @RequestMapping(value = {"/treeData", ""})
    public Object treeData() throws Exception {
        return service.list();
    }

    @ResponseBody
    @GetMapping(value = {"/getListAll"})
    public Object getListAll() throws Exception {
        List<Role> roleList = service.list();
        roleList.forEach(role -> role.setResourceNames(resourceService.getResourceNameByIds(SplitUtil.split2LongList(role.getResourceIds()))));
        return buildSuccessResponse();
    }

    @ResponseBody
    @GetMapping(value = {"/getList"})
    public Object getList(RoleVo vo) throws Exception {
        IPage<Role> page = initJqGridPage(vo);
        LambdaQueryWrapper<Role> queryWrapper = initQuery(vo);
        if (StringUtils.isNotBlank(vo.getName())) {
            queryWrapper.like(Role::getName, vo.getName());
        }
        page = service.page(page, queryWrapper);
        return jqGridPageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Role form = service.getById(id);
        if (form == null) {
            form = new Role();
        }
        form.setResourceNames(resourceService.getResourceNameByIds(SplitUtil.split2LongList(form.getResourceIds())));
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @ResponseBody
    @PostMapping(value = {"/form"})
    public Object formPost(Role form) throws Exception {
        boolean result = service.saveOrUpdate(form);
        return buildSuccessResponse(result);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public Object delete(String id) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        return buildSuccessResponse(result);
    }

}

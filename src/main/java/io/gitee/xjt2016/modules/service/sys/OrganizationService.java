package io.gitee.xjt2016.modules.service.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.sys.Organization;
import io.gitee.xjt2016.modules.mapper.sys.OrganizationMapper;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService extends ServiceImpl<OrganizationMapper, Organization> {
}


package io.gitee.xjt2016.modules.gen.domain;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

@Data
public class GenTableColumn {

    private String colName;
    private String colType;
//    private int colSqlType;
    private String colComment;

    /**
     * 字段长度
     */
    private Long precision;
    /**
     * 字段小数位（float,double)
     */
    private int scale;

    /**
     * 是否主键
     */
    private boolean isPK = false;
    /**
     * 是否自动增长
     */
    private boolean isAutoIncrement = false;

    /**
     * 字段是否可以为空
     */
    private boolean nullable;
    /**
     * 字段可为空时的缺省
     */
    private String defaultValue;

    private String javaFieldName;

    private String javaFieldType;

    public String getJavaFieldSimpleType() {
        return StringUtils.substringAfterLast(this.getJavaFieldType(), ".");
    }

}

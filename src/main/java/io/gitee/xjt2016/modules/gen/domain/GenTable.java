package io.gitee.xjt2016.modules.gen.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
public class GenTable {
    private String tableName;

    private String entityName;

    private String basePackage = "io.gitee.xjt2016";

    private List<GenTableColumn> columns = new ArrayList<>();

    public GenTable(String tableName) {
        this.tableName = tableName;
    }
}

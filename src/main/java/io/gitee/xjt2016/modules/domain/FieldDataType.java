package io.gitee.xjt2016.modules.domain;

import lombok.Getter;

@Getter
public enum FieldDataType {
    String("String", "String"),
    Number("Number", "Number"),
    Boolean("Boolean", "Boolean"),
    Object("Object", "Object"),
    Array("Array", "Array"),
    Function("Function", "Function"),
    RegExp("RegExp", "RegExp"),
    Null("Null", "Null");

    private final String name;
    private final String value;

    FieldDataType(String name, String value) {
        this.name = name;
        this.value = value;
    }
}

package io.gitee.xjt2016.modules.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.Resource;

public interface ResourceMapper extends BaseMapper<Resource> {
}

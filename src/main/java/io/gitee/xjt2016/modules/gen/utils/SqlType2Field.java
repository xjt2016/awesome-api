/**
 *
 */
package io.gitee.xjt2016.modules.gen.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class SqlType2Field {

    public final static Map<Integer, String> sqlType2Field = new HashMap<>();

    static {
        sqlType2Field.put(Types.VARCHAR, "String");
        sqlType2Field.put(Types.DATE, "Date");    //java.util.Date
        //mysql
//		sqltype2Feild.put(Types.BIT, "Boolean");
        sqlType2Field.put(Types.BIT, "Integer");
        sqlType2Field.put(Types.TINYINT, "Integer");
        sqlType2Field.put(Types.SMALLINT, "Integer");
        sqlType2Field.put(Types.INTEGER, "Integer");
        sqlType2Field.put(Types.BIGINT, "Long");
        sqlType2Field.put(Types.REAL, "Float");
        sqlType2Field.put(Types.DOUBLE, "Double");

        sqlType2Field.put(Types.CHAR, "String");
        sqlType2Field.put(Types.LONGVARCHAR, "String");

        sqlType2Field.put(Types.BINARY, "Blob");    //java.sql.Blob
        sqlType2Field.put(Types.VARBINARY, "Blob");    //java.sql.Blob
        sqlType2Field.put(Types.LONGVARBINARY, "Blob");    //java.sql.Blob

        sqlType2Field.put(Types.TIME, "Date");    //java.util.Date
        sqlType2Field.put(Types.TIMESTAMP, "Date");    //java.util.Date
        //oracle
        sqlType2Field.put(Types.NUMERIC, "Long");
        sqlType2Field.put(Types.FLOAT, "BigDecimal");    //java.math.BigDecimal
        sqlType2Field.put(Types.DECIMAL, "BigDecimal");    //java.math.BigDecimal
        sqlType2Field.put(Types.CLOB, "Clob");    //java.sql.Clob
    }

    public static String mapJavaType(int sqlType) {
        String javaType = sqlType2Field.get(sqlType);
        if (StringUtils.isNotBlank(javaType)) {
            return javaType;
        } else {
            log.error("字段没有对应的Java类型，SQL_TYPE：" + sqlType);
            return "String";
        }
    }
}

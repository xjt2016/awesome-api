package io.gitee.xjt2016.modules.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.sys.User;
import io.gitee.xjt2016.modules.domain.vo.UserVo;
import io.gitee.xjt2016.modules.service.sys.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController {

    public static final String module = "user";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    UserService service;

    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @GetMapping
    public String index() throws Exception {
        return pathPrefix + "/list";
    }

    @ResponseBody
    @RequestMapping(value = {"/treeData", ""})
    public Object treeData() throws Exception {
        return service.list();
    }

    @ResponseBody
    @GetMapping(value = {"/getListAll"})
    public Object getListAll() throws Exception {
        return buildSuccessResponse(service.list());
    }

    @ResponseBody
    @GetMapping(value = {"/getList"})
    public Object getList(UserVo vo) throws Exception {
        IPage<User> page = initJqGridPage(vo);
        LambdaQueryWrapper<User> queryWrapper = initQuery(vo);

        if (StringUtils.isNotBlank(vo.getUsername())) {
            queryWrapper.like(User::getUsername, vo.getUsername());
        }
        page = service.page(page, queryWrapper);
        return jqGridPageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        User form = service.getById(id);
        if (form == null) {
            form = new User();
        }
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @ResponseBody
    @PostMapping(value = {"/form"})
    public Object formPost(User form) throws Exception {
        boolean result = service.saveOrUpdate(form);
        return buildResponse(result);
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public Object delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildResponse(result);
    }

}

package io.gitee.xjt2016.modules.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.sys.User;

public interface UserMapper extends BaseMapper<User> {
}

package io.gitee.xjt2016.modules.domain.vo;

import io.gitee.xjt2016.modules.common.BootStrapTablePage;
import io.gitee.xjt2016.modules.common.config.JqGridTablePage;
import lombok.Data;

@Data
public class RoleVo extends JqGridTablePage {

    private String name;
}

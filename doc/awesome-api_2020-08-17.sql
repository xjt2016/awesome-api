# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31-log)
# Database: awesome-api
# Generation Time: 2020-08-16 17:58:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table field
# ------------------------------------------------------------

DROP TABLE IF EXISTS `field`;

CREATE TABLE `field` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `fid` bigint(11) DEFAULT '0',
  `pid` bigint(11) DEFAULT '0',
  `interface_id` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT '参数名',
  `data_type` varchar(64) DEFAULT NULL COMMENT '参数类型',
  `default_value` varchar(64) DEFAULT NULL COMMENT '参数的默认值',
  `required` tinyint(2) DEFAULT '0' COMMENT '参数是否必须传',
  `description` varchar(128) DEFAULT NULL COMMENT '描述',
  `type` varchar(8) DEFAULT NULL COMMENT '参数类型，请求，响应',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `field` WRITE;
/*!40000 ALTER TABLE `field` DISABLE KEYS */;

INSERT INTO `field` (`id`, `fid`, `pid`, `interface_id`, `name`, `data_type`, `default_value`, `required`, `description`, `type`)
VALUES
	(500,2,0,1,'name','Number','0',0,'描述','REQUEST'),
	(501,3,0,1,'Item 1','Object','String',0,NULL,'REQUEST'),
	(502,4,0,1,'Item 1','String','String',0,NULL,'REQUEST'),
	(503,5,0,1,'Item 1','String','String',0,NULL,'REQUEST'),
	(504,6,3,1,'Item 1','Object','String',0,NULL,'REQUEST'),
	(505,7,6,1,'Item 1','Object','String',0,NULL,'REQUEST'),
	(506,8,7,1,'Item 1','Object','String',0,NULL,'REQUEST'),
	(507,25,8,1,'Item 1','Object','String',0,NULL,'REQUEST'),
	(508,1,0,1,'childNum','Number',NULL,0,'','RESPONSE'),
	(509,2,0,1,'buyerNick','String',NULL,0,'','RESPONSE'),
	(510,3,0,1,'created','Number',NULL,0,'','RESPONSE'),
	(511,4,0,1,'receiverName','String',NULL,0,'','RESPONSE'),
	(512,5,0,1,'postFee','Array',NULL,0,'','RESPONSE'),
	(513,6,0,1,'receiverMobile','String',NULL,0,'','RESPONSE'),
	(514,7,0,1,'isPreSale','Number',NULL,0,'','RESPONSE'),
	(515,8,0,1,'receiverProvince','String',NULL,0,'','RESPONSE'),
	(516,9,0,1,'receiverCounty','String',NULL,0,'','RESPONSE'),
	(517,10,0,1,'receiverCity','String',NULL,0,'','RESPONSE'),
	(518,11,0,1,'tid','String',NULL,0,'','RESPONSE'),
	(519,12,0,1,'receiverAddress','String',NULL,0,'','RESPONSE'),
	(520,13,0,1,'buyerMessage','String',NULL,0,'','RESPONSE'),
	(521,14,0,1,'consignTime','Number',NULL,0,'','RESPONSE'),
	(522,15,0,1,'sysDiscountFee','String',NULL,0,'','RESPONSE'),
	(523,16,0,1,'totalFee','String',NULL,0,'','RESPONSE'),
	(524,17,0,1,'noLogisticsSend','Boolean',NULL,0,'','RESPONSE'),
	(525,18,0,1,'cod','Number',NULL,0,'','RESPONSE'),
	(526,19,0,1,'modified','Number',NULL,0,'','RESPONSE'),
	(527,20,0,1,'orders','Array',NULL,0,'','RESPONSE'),
	(528,21,20,1,'itemType','String',NULL,0,'','RESPONSE'),
	(529,22,20,1,'orderId','String',NULL,0,'','RESPONSE'),
	(530,23,20,1,'skuUrl','String',NULL,0,'','RESPONSE'),
	(531,24,20,1,'skuShortProperties','String',NULL,0,'','RESPONSE'),
	(532,25,20,1,'isPreSale','Number',NULL,0,'','RESPONSE'),
	(533,26,20,1,'title','String',NULL,0,'','RESPONSE'),
	(534,27,20,1,'saleProps','Array',NULL,0,'','RESPONSE'),
	(535,28,27,1,'value','String',NULL,0,'','RESPONSE'),
	(536,29,27,1,'key','String',NULL,0,'','RESPONSE'),
	(537,30,20,1,'picPath','String',NULL,0,'','RESPONSE'),
	(538,31,20,1,'itemId','String',NULL,0,'','RESPONSE'),
	(539,32,20,1,'number','Number',NULL,0,'','RESPONSE'),
	(540,33,20,1,'skuProperties','String',NULL,0,'','RESPONSE'),
	(541,34,20,1,'outerSkuId','String',NULL,0,'','RESPONSE'),
	(542,35,20,1,'totalFee','String',NULL,0,'','RESPONSE'),
	(543,36,20,1,'price','String',NULL,0,'','RESPONSE'),
	(544,37,20,1,'orderNoLogisticsSend','Boolean',NULL,0,'','RESPONSE'),
	(545,38,20,1,'outerId','String',NULL,0,'','RESPONSE'),
	(546,39,20,1,'payment','String',NULL,0,'','RESPONSE'),
	(547,40,20,1,'skuId','String',NULL,1,'','RESPONSE'),
	(548,41,20,1,'status','String',NULL,1,'','RESPONSE'),
	(549,42,0,1,'payment','String',NULL,0,'','RESPONSE'),
	(550,43,0,1,'sellerMemo','String',NULL,0,'','RESPONSE'),
	(551,44,0,1,'status','String',NULL,0,'','RESPONSE'),
	(552,45,20,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(553,46,20,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(554,47,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(555,48,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(556,49,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(557,50,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(558,51,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(559,52,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(560,53,5,1,'Item 1','String','String',0,NULL,'RESPONSE'),
	(570,1,0,2,'data','Object',NULL,0,'','RESPONSE'),
	(571,2,1,2,'orderStatistical','Object',NULL,0,'','RESPONSE'),
	(572,3,2,2,'monthOnlineOrderNumber','Number',NULL,0,'','RESPONSE'),
	(573,4,2,2,'weekStoreOrderNumber','Number',NULL,0,'','RESPONSE'),
	(574,5,2,2,'weekOfflineOrderNumber','Number',NULL,0,'','RESPONSE'),
	(575,6,2,2,'weekOnlineOrderNumber','Number',NULL,0,'','RESPONSE'),
	(576,7,2,2,'lastMonth','Number',NULL,0,'','RESPONSE'),
	(577,8,2,2,'monthStoreOrderNumber','Number',NULL,0,'','RESPONSE'),
	(578,9,2,2,'lastWeek','Number',NULL,0,'','RESPONSE'),
	(579,10,2,2,'monthOfflineOrderNumber','Number',NULL,0,'','RESPONSE'),
	(580,11,1,2,'saleAmountStatistical','Object',NULL,0,'','RESPONSE'),
	(581,12,11,2,'weekOnlineOrderAmount','Number',NULL,0,'','RESPONSE'),
	(582,13,11,2,'weekOfflineOrderAmount','Number',NULL,0,'','RESPONSE'),
	(583,14,11,2,'weekStoreOrderAmount','Number',NULL,0,'','RESPONSE'),
	(584,15,11,2,'lastMonth','Number',NULL,0,'','RESPONSE'),
	(585,16,11,2,'monthStoreOrderAmount','Number',NULL,0,'','RESPONSE'),
	(586,17,11,2,'monthOnlineOrderAmount','Number',NULL,0,'','RESPONSE'),
	(587,18,11,2,'lastWeek','Number',NULL,0,'','RESPONSE'),
	(588,19,11,2,'monthOfflineOrderAmount','Number',NULL,0,'','RESPONSE'),
	(589,20,0,2,'status','Number',NULL,0,'','RESPONSE'),
	(590,22,0,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(591,24,0,2,'Item 1','Object','String',0,NULL,'REQUEST'),
	(592,26,0,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(593,28,0,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(594,30,0,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(595,31,24,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(596,32,24,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(597,33,24,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(598,34,24,2,'Item 1','String','String',0,NULL,'REQUEST'),
	(599,35,24,2,'Item 1','String','String',0,NULL,'REQUEST');

/*!40000 ALTER TABLE `field` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table function
# ------------------------------------------------------------

DROP TABLE IF EXISTS `function`;

CREATE TABLE `function` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `function` WRITE;
/*!40000 ALTER TABLE `function` DISABLE KEYS */;

INSERT INTO `function` (`id`, `project_id`, `name`, `description`)
VALUES
	(1,1,'订单模块',NULL),
	(6,1,'用户模块','');

/*!40000 ALTER TABLE `function` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table interface
# ------------------------------------------------------------

DROP TABLE IF EXISTS `interface`;

CREATE TABLE `interface` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `function_id` int(11) DEFAULT '-1',
  `name` varchar(64) DEFAULT '',
  `url` varchar(64) DEFAULT '',
  `description` varchar(256) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;

INSERT INTO `interface` (`id`, `function_id`, `name`, `url`, `description`)
VALUES
	(1,1,'interface','1','1'),
	(2,1,'2222','2222','');

/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` (`id`, `name`, `url`)
VALUES
	(1,'项目','/project'),
	(3,'功能','/function'),
	(4,'接口','/interface'),
	(5,'字段','/field'),
	(6,'参数','/parameter'),
	(7,'资源','/resource');

/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table param
# ------------------------------------------------------------

DROP TABLE IF EXISTS `param`;

CREATE TABLE `param` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `interface_id` int(11) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table parameter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parameter`;

CREATE TABLE `parameter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;

INSERT INTO `parameter` (`id`, `key`, `value`)
VALUES
	(1,'1',NULL),
	(2,'12','12'),
	(3,'123','123');

/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `name`, `description`)
VALUES
	(1,'12','项目描述'),
	(2,'3','项目描述'),
	(3,'3','项目描述'),
	(4,'4','项目描述'),
	(5,'5','项目描述项目描述项目描述项'),
	(6,'6','项目描述'),
	(7,'7','项目描述'),
	(8,'8','项目描述'),
	(9,'8','项目描述'),
	(10,'9','项目描述'),
	(11,'12','12'),
	(12,'123','关于项目'),
	(13,'awesome-cloud-feign','awesome-cloud-feign');

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `resource`;

CREATE TABLE `resource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `desc` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `thumbnail` varchar(128) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;

INSERT INTO `resource` (`id`, `name`, `desc`, `url`, `thumbnail`, `type`, `created`, `modified`)
VALUES
	(1,'Skywalking','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(2,'Kibana','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(3,'Raycloud慢SQL','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(4,'Api调用统计','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(5,'聚石塔','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(6,'京东云顶','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(7,'有赞云','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(8,'放心购开放平台','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(9,'微信小商店开放平台','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(10,'快递助手TJ','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(11,'发布系统','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(12,'SQL审核系统','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(13,'SQL查询系统','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(14,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(15,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(16,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(17,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(18,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(19,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(20,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(21,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(22,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(23,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(24,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(25,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(26,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL),
	(27,'12','12','https://www.baidu.com',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

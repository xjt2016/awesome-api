package io.gitee.xjt2016.modules.sysLog;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Log4j2
@Component
public class DefaultSysLogConsumer implements Consumer<SysLogInfo> {
    @Override
    public void accept(SysLogInfo sysLogInfo) {
        log.info(sysLogInfo);
    }
}

#!/bin/bash

USER=$(whoami)
PROJECT_NAME=awesome-api
# | grep $user
pid=$(ps -f -U${USER}|grep java | grep ${PROJECT_NAME} | grep -v "grep" | awk '{print $2}')

echo "正在停止进程${pid}"
exitFlag=0
if [[ ! -n "$pid" ]];then
    echo "不存在java进程"
else
    kill ${pid}
    sleep 5
    count=$(ps -f -U${USER}|grep java | grep ${PROJECT_NAME} | grep -v "grep" | wc -l)
    if [[ "$count" -gt 0 ]];then
        echo "停止java进程${pid}'}失败"
        exitFlag=-1
    else
        echo "成功停止java进程${pid}"
    fi
fi
exit ${exitFlag}
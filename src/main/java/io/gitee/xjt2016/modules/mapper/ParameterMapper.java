package io.gitee.xjt2016.modules.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitee.xjt2016.modules.domain.Parameter;

public interface ParameterMapper extends BaseMapper<Parameter> {
}

package io.gitee.xjt2016.modules.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.gitee.xjt2016.modules.common.BaseController;
import io.gitee.xjt2016.modules.domain.Resource;
import io.gitee.xjt2016.modules.domain.vo.ResourceVo;
import io.gitee.xjt2016.modules.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/resource")
public class ResourceController extends BaseController {

    public static final String module = "resource";
    public static final String pathPrefix = "/" + module;
    private static final String PARAM_KEY = "data";

    @Autowired
    ResourceService service;


    @ModelAttribute
    public void modelAttribute(Model model, HttpServletRequest request) {
        model.addAttribute("module", module);
        model.addAttribute("pathPrefix", pathPrefix);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        //model.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        if (map != null) {
            model.addAttribute("message", map.get("message") + "");
            model.addAttribute("code", map.get("code") + "");
        }
    }

    @RequestMapping(value = {"/index", ""})
    public String index() throws Exception {
        return pathPrefix + "/index";
    }

    @ResponseBody
    @GetMapping(value = {"/getTreeListAll"})
    public Object getTreeListAll(String selectIds) throws Exception {
        List<Long> selectIdList = split2LongList(selectIds);
        return service.list().stream().map(resource -> {
            Map<String, Object> map = BeanUtil.beanToMap(resource);
            map.put("parentId", resource.getPid());
            map.put("checked", selectIdList.contains(resource.getId()));
            return map;
        }).collect(Collectors.toList());
    }

    @ResponseBody
    @GetMapping(value = {"/getListAll"})
    public Object getListAll() throws Exception {
        return buildSuccessResponse(service.list());
    }

    @ResponseBody
    @RequestMapping(value = {"/getList"})
    public Object getList(ResourceVo vo) throws Exception {
        IPage<Resource> page = initMyBatisPage(vo);
        page = service.page(page, Wrappers.lambdaQuery(Resource.class));
        return pageResponse(page);
    }

    @GetMapping(value = {"/form"})
    public String formGet(Long id, Model model) throws Exception {
        Resource form = null;
        if (id != null) {
            form = service.getById(id);
        }
        if (form == null) {
            form = new Resource();
        }
        model.addAttribute(PARAM_KEY, form);
        return pathPrefix + "/form";
    }

    @ResponseBody
    @GetMapping(value = {"/delete"})
    public ModelAndView delete(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        attributes.addFlashAttribute("message", result);
        attributes.addFlashAttribute("code", 100);
        return buildRedirectModelAndView(pathPrefix);
    }

    @ResponseBody
    @PostMapping(value = {"/form"})
    public Object formPost(Resource form, RedirectAttributes attributes) throws Exception {
        boolean result = service.saveOrUpdate(form);
        return buildResponse(result);
    }

    @PostMapping(value = "/saveAll")
    @ResponseBody
    public Object saveAll(String params) {
        List<Resource> organizationList = JSON.parseArray(params, Resource.class);
        boolean result = service.saveOrUpdateBatch(organizationList);
        return buildSuccessResponse(result);
    }

    @ResponseBody
    @PostMapping(value = {"/delete"})
    public Object deletePost(String id, RedirectAttributes attributes) throws Exception {
        boolean result = service.removeByIds(split2LongList(id));
        return buildResponse(result);
    }

}

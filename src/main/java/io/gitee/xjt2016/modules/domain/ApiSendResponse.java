package io.gitee.xjt2016.modules.domain;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ApiSendResponse {
    private String url;
    private int status;
    private List<Map<String, Object>> headers;
    private String cookies;
    private String body;

    private String charset;

    private String httpVersion;

    private boolean isGzip;

    private boolean isOk;
}

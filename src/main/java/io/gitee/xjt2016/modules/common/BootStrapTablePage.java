package io.gitee.xjt2016.modules.common;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BootStrapTablePage implements Serializable {

    private static final long serialVersionUID = -7143281720625535535L;
    private Integer pageSize = 10;

    private Integer pageNo = 1;

    private String sortName = "modified";

    private String sortOrder = "desc";

    private String searchText;
}

package io.gitee.xjt2016.modules.common.utils;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SplitUtil {

    public static List<Long> split2LongList(String str) {
        Preconditions.checkArgument(str != null, "待分割的字符串不能为空");
        return Arrays.stream(str.split("[,，]"))
                .filter(StringUtils::isNotBlank)
                .map((Function<String, Long>) Long::valueOf)
                .collect(Collectors.toList());
    }
}

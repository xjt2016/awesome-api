package io.gitee.xjt2016.modules.common.config;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JqGridTablePage {
    private Integer page;//当前页码
    private Integer rows;//每页显示记录数

    private Boolean _search = false;
    private String nd;
    private String sidx;
    private String sord;
}

package io.gitee.xjt2016.modules.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.Interface;
import io.gitee.xjt2016.modules.mapper.InterfaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class InterfaceService extends ServiceImpl<InterfaceMapper, Interface> {

    @Autowired
    FunctionService functionService;

    public Interface getInterface(Long interfaceId) {
        Interface anInterface = this.getBaseMapper().selectById(interfaceId);
        if (anInterface != null) {
            anInterface.setFunction(functionService.getFunction(anInterface.getFunctionId()));
        }
        return anInterface;
    }

    public List<Interface> getInterfaceListByFunctionIds(Collection<Long> functionIds) {
        LambdaQueryWrapper<Interface> queryWrapper = Wrappers.lambdaQuery(Interface.class);
        queryWrapper.in(Interface::getFunctionId, functionIds);
        return this.baseMapper.selectList(queryWrapper);
    }
}

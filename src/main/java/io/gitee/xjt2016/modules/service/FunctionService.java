package io.gitee.xjt2016.modules.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.gitee.xjt2016.modules.domain.Function;
import io.gitee.xjt2016.modules.mapper.FunctionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctionService extends ServiceImpl<FunctionMapper, Function> {

    @Autowired
    ProjectService projectService;

    public Function getFunction(Long functionId) {
        Function function = this.getBaseMapper().selectById(functionId);
        if (function != null) {
            function.setProject(projectService.getById(function.getProjectId()));
        }
        return function;
    }

    public List<Function> getFunctionByProjectId(Long projectId) {
        LambdaQueryWrapper<Function> queryWrapper = Wrappers.lambdaQuery(Function.class);
        queryWrapper.eq(Function::getProjectId, projectId);
        return this.baseMapper.selectList(queryWrapper);
    }

}

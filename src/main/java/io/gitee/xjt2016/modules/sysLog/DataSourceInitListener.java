package io.gitee.xjt2016.modules.sysLog;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 项目启动成功后回调
 */
@Component
@Log4j2
public class DataSourceInitListener implements ApplicationListener<ContextRefreshedEvent> {

    @SuppressWarnings("unchecked")
    @Override
    public void onApplicationEvent(ContextRefreshedEvent ev) {
        //防止重复执行。
        if (ev.getApplicationContext().getParent() == null) {
            log.info("ev.....");
        }
    }

}
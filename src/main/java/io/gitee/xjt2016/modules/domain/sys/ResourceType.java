package io.gitee.xjt2016.modules.domain.sys;

public enum ResourceType {
    menu("菜单"), button("按钮");

    private final String info;

    ResourceType(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}